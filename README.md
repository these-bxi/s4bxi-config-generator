# S4BXI config generator

Simple [Deno](https://deno.land/) (type)script to generate an XML deploy file, to be fed to an S4BXI simulation (generating platforms is not yet possible, it should be in the future)

## Requirements

Install (or simply download, it's portable) Deno: [deno.land/#installation](https://deno.land/#installation)

If you don't like Deno, pre-built binaries with zero dependencies are generated for Linux in our [CI](https://framagit.org/s4bxi/s4bxi-config-generator/-/jobs/artifacts/master/browse?job=build)

## Usage

`deno run ./genDeploy.ts [options]`

The resulting XML is written to the standard output, so that you can easily redirect it wherever you want, for example:  
`deno run ./genDeploy.ts -n host1 > deploy.xml`

If you downloaded pre-built binaries, the command becomes `./S4BXI_genDeploy_Linux [options]` (make sure the file is executable, `chmod +x S4BXI_genDeploy_Linux`)

### Options

- **--node [-n]** *(default: `[]`, but you really want to specify this yourself)*: Hostname of your SimGrid Host (from the XML platform file), you should specify the *main host* name and not the *NIC* name. Can be specified several times to include several nodes. Another way of quickly including many nodes is to use the dedicated format: hostnames can be separated by commas or use a range of number, for example `-n "host1..3,host5-6,host42"` will create hosts *"host1"*, *"host2"*,  *"host3"*, *"host5"*, *"host6"* and *"host42"*

- **--use-real-memory [-u]** *(default: `true`)*: Option forwarded to each *user_app* actor (boolean)

- **--model-pci [-m]** *(default: `true`)*: Option forwarded to each *user_app* actor (boolean)

- **--VN** *(default: `[1,3]`)*: ID of a VN that should be used by actors on the NIC of each node (so 1, 2, 3 or 4). Can (and probably should) be specified several times if several VNs are used (you'll probably need at least two: 1 and 3 or 2 and 4, to be able to send as well as receive messages)

- **--initiator-number [-i]** *(default: `2`)*: Number of *NicInitiator* actors to instanciate on each node **for each VN** (so `-i 3` with 2 VNs creates 6 actors for each node)

- **--target-number [-t]** *(default: `2`)*: Number of *NicTarget* actors to instanciate on each node **for each VN** (so `-t 3` with 2 VNs creates 6 actors)

- **--e2e-number [-e]** *(default: `1`)*: Number of *NicE2E* actors to instanciate on each node (E2E actors are not linked with a specific VN, and having only one should in theory always be fine)

- **--rank-argument [-ra]** *(default: `""`)*: If specified, an argument with this name will be passed to all *user_app* actors and incremented for each one of them (starting at zero). For example, when specifying `--rank-argument="-r"`, actors will be invoked with `-r 0`, `-r 1`, etc.