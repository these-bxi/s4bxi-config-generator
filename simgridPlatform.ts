// Number with potentially an indicator on the order of magnitude. Ex: 42 ; 42G ; 42n
export type MagnitudeNumber = number | string;

export class Prop {
  id: string;
  value: any;

  constructor(id: string, value: any) {
    this.id = id;
    this.value = value;
  }
}

export class Host {
  name: string;
  /** unit = Flop/s */
  speed: MagnitudeNumber;
  props: Prop[];

  constructor(name: string, speed: MagnitudeNumber, props: Prop[] = []) {
    this.name = name;
    this.speed = speed;
    this.props = props;
  }

  toString(): string {
    return `<host id="${this.name}" speed="${this.speed}f"/>`;
  }
}

export class Link {
  /** unit = B/s */
  bandwidth: MagnitudeNumber;
  /** unit = s */
  latency: MagnitudeNumber;

  constructor(bandwidth: MagnitudeNumber, latency: MagnitudeNumber) {
    this.bandwidth = bandwidth;
    this.latency = latency;
  }
}

const exponentUnits: Record<string, number> = {
  Y: 1e21,
  E: 1e18,
  P: 1e15,
  T: 1e12,
  G: 1e9,
  M: 1e6,
  k: 1e3,
  m: 1e-3,
  u: 1e-6,
  n: 1e-9,
  p: 1e-12,
};

export const magnitudeToNumber = (n: MagnitudeNumber) => {
  if (typeof n == "number") return n;

  const unit = n.slice(-1);

  return exponentUnits[unit]
    ? (parseFloat(n.slice(0, -1)) * exponentUnits[unit])
    : parseFloat(n);
};
