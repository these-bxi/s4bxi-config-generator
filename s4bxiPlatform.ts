import { indent, numberMagnitudeFormat } from "./util.ts";
import {
  Host,
  Link,
  MagnitudeNumber,
  magnitudeToNumber,
  Prop,
} from "./simgridPlatform.ts";

interface BXIHardware {
  name: string;
}

interface ComputeHardware {
  name: string;
}

export class Router implements BXIHardware {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  toString(): string {
    return `<router id="${this.name}"/>`;
  }
}

export class NICHost extends Host implements BXIHardware {
  constructor(name: string, speed: MagnitudeNumber = "1G", props: Prop[] = []) {
    super(name, speed, props);
  }
}

export class CPUHost extends Host implements ComputeHardware {
  constructor(
    name: string,
    speed: MagnitudeNumber = "10G",
    props: Prop[] = [],
  ) {
    super(name, speed, props);
  }
}

export class BXILink extends Link {
  port1: BXIHardware;
  port2: BXIHardware;
  multiplier: number;

  constructor(port1: BXIHardware, port2: BXIHardware) {
    super("11.1G", "500n");

    this.port1 = port1;
    this.port2 = port2;
    this.multiplier = 1;
  }

  routeString(): string {
    const addFatLink = this.multiplier > 1 ? `\n    <link_ctn id="${this.port1.name}_${this.port2.name}_FAT" />` : '';

    return `<route src="${this.port1.name}" dst="${this.port2.name}" symmetrical="NO">
    <link_ctn id="${this.port1.name}_${this.port2.name}" direction="DOWN" />${addFatLink}
</route>
<route src="${this.port2.name}" dst="${this.port1.name}" symmetrical="NO">
    <link_ctn id="${this.port1.name}_${this.port2.name}" direction="UP" />${addFatLink}
</route>
`;
  }

  toString(): string {
    if (this.multiplier > 1) {
      return `<link id="${this.port1.name}_${this.port2.name}_FAT" bandwidth="${this.bandwidth}Bps" latency="${this.latency}s" sharing_policy="FATPIPE"/>
<link id="${this.port1.name}_${this.port2.name}" bandwidth="${
        numberMagnitudeFormat(
          magnitudeToNumber(this.bandwidth) * this.multiplier,
        )
      }Bps" latency="0ns" sharing_policy="SPLITDUPLEX"/>`;
    }

    return `<link id="${this.port1.name}_${this.port2.name}" bandwidth="${this.bandwidth}Bps" latency="${this.latency}s" sharing_policy="SPLITDUPLEX"/>`;
  }
}

export class PCILink extends Link {
  nic: NICHost;
  compute: ComputeHardware[];
  singleUserBandwidth: MagnitudeNumber;

  constructor(nic: NICHost, compute: CPUHost[]) {
    super("15.75G", "250n");

    this.nic = nic;
    this.compute = compute;
    this.singleUserBandwidth = "11.1G";
  }

  routeString(): string {
    const linkCtnString = `<link_ctn id="${this.nic.name}_PCI_FAT" />
<link_ctn id="${this.nic.name}_PCI" />`;

    return this.compute.map(({ name }) =>
      `<route src="${name}" dst="${this.nic.name}">
${indent(linkCtnString)}
</route>
`
    ).join("\n");
  }

  toString(): string {
    return `<link id="${this.nic.name}_PCI_FAT" bandwidth="${this.singleUserBandwidth}Bps" latency="0ns" sharing_policy="FATPIPE"/>
<link id="${this.nic.name}_PCI" bandwidth="${this.bandwidth}Bps" latency="${this.latency}s" sharing_policy="SHARED"/>`;
  }
}

export class PlatformNode {
  nodename: string;
  nic: NICHost;
  compute: CPUHost[] = [];
  pci: PCILink;

  constructor(nodename: string, cores: number = 1) {
    this.nodename = nodename;
    this.nic = new NICHost(nodename + "_NIC");
    for (let i = 0; i < cores; ++i) this.compute.push(new CPUHost(`${nodename}_CPU${i}`));
    this.pci = new PCILink(this.nic, this.compute);
  }

  toString(): string {
    return `<!-- Node ${this.nodename} -->
${this.compute.map(c => c.toString()).join('\n')}
${this.nic.toString()}`;
  }
}

export class Zone {
  nodes: Map<string, PlatformNode>;
  links: BXILink[] = [];
  routers: Map<string, Router>;
  id: string;
  routing: string;

  constructor(
    id: string = "",
    routing: string = "Floyd",
  ) {
    this.nodes = new Map<string, PlatformNode>();
    this.routers = new Map<string, Router>();
    this.id = id.length ? id : "zone" + (Math.round(10000 * Math.random()));
    this.routing = routing;
  }

  addNode(n: PlatformNode) {
    this.nodes.set(n.nodename, n);
  }

  addRouter(r: Router) {
    this.routers.set(r.name, r);
  }

  addLink(port1: NICHost | Router, port2: NICHost | Router) {
    // Look for similar link first
    let l: BXILink;
    for (let i = 0; i < this.links.length; ++i) {
      l = this.links[i];
      if (
        l.port1.name === port1.name && l.port2.name === port2.name ||
        l.port1.name === port2.name && l.port2.name === port1.name
      ) {
        // Same link, possibly in the other direction, increment multiplier and don't re-create link
        ++l.multiplier;
        return;
      }
    }

    // New link, create it
    this.links.push(new BXILink(port1, port2));
  }

  connect(p1: string, p2: string) {
    const port1 = this.nodes.get(p1)?.nic || this.routers.get(p1);
    const port2 = this.nodes.get(p2)?.nic || this.routers.get(p2);
    if (!port1) {
      throw `Can't connect non-existing machine ${p1}`;
    } else if (!port2) {
      throw `Can't connect non-existing machine ${p2}`;
    } else {
      this.addLink(port1, port2);
    }
  }

  toString(): string {
    let zoneString = "";
    // Hosts
    this.nodes.forEach((
      node: PlatformNode,
    ) => (zoneString += node.toString() + "\n\n"));

    this.routers.forEach((
      router: Router,
    ) => (zoneString += router.toString() + "\n"));

    zoneString += "\n";

    this.nodes.forEach((
      node: PlatformNode,
    ) => (zoneString += node.pci.toString() + "\n\n"));

    this.links.forEach((
      link: BXILink,
    ) => (zoneString += link.toString() + "\n"));

    zoneString += "\n";

    this.nodes.forEach((
      node: PlatformNode,
    ) => (zoneString += node.pci.routeString() + "\n"));

    this.links.forEach((
      link: BXILink,
    ) => (zoneString += link.routeString() + "\n"));

    return `<zone id="${this.id}" routing="${this.routing}">
${indent(zoneString)}</zone>`;
  }
}

export class S4BXIPlatform {
  zones: Zone[];

  constructor(zones: Zone[]) {
    this.zones = zones;
  }

  toString(): string {
    let zonesString = "";
    this.zones.map((zone: Zone) => (zonesString += zone.toString() + "\n"));

    return `<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">

<platform version="4.1">
    <config>
        <prop id="network/model" value="CM02" />
        <prop id="network/loopback-lat" value="0.000000001" />
        <prop id="network/loopback-bw" value="99000000000" />
    </config>
${indent(zonesString)}</platform>`;
  }
}

export default {/* TODO */};
