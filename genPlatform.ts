import { Args, parse } from "https://deno.land/std@0.79.0/flags/mod.ts";
import { parseHostnameRange } from "./util.ts";
import { PlatformNode, Router, S4BXIPlatform, Zone } from "./s4bxiPlatform.ts";

const args: Args = parse(Deno.args, {
  alias: {
    node: "n",
    cores: "c",
  },
  string: ["node"],
  default: {
    node: [],
    cores: 1,
  },
});

const zone = new Zone();

zone.addRouter(new Router("wmc10100"));

const makeNode = (node: string): PlatformNode => new PlatformNode(node, args.cores);

const addNodes = (zone: Zone, str: string) =>
  parseHostnameRange(str).map((s: string) => {
    zone.addNode(makeNode(s));
    zone.connect("wmc10100", s);
  });

if (Array.isArray(args.node)) {
  args.node.map((n: string) => addNodes(zone, n));
} else {
  addNodes(zone, args.node);
}

console.log(new S4BXIPlatform([zone]).toString());
