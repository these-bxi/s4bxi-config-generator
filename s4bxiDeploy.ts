let rankCount = 0;

export interface ActorProperty {
  id: string;
  value: any;
}

export enum ActorFunction {
  UserApp = "user_app",
  NicInitiator = "nic_initiator",
  NicTarget = "nic_target",
  NicE2E = "nic_e2e",
}

export class Actor {
  host: string;
  function: ActorFunction;
  props: ActorProperty[];
  args: string[] = [];

  constructor(
    host: string,
    func: ActorFunction,
    props: ActorProperty[],
    args: string[] = [],
  ) {
    this.host = host;
    this.function = func; // bare "function" is a reserved keyword
    this.props = props;
    this.args = args;
  }

  toString(): string {
    const props = this.props.reduce(
      (str, prop) =>
        str + `    <prop id="${prop.id}" value="${prop.value}"/>\n`,
      "\n",
    );
    const args = this.args.reduce(
      (str, arg) => str + `    <argument value="${arg}"/>\n`,
      "",
    );

    return this.props.length || this.args.length
      ? `<actor host="${this.host}" function="${this.function}">${props}${args}</actor>`
      : `<actor host="${this.host}" function="${this.function}"/>`;
  }
}

export class DeployNode {
  main_host: string;
  use_VNs: number[];
  process_per_node: number;
  initiator_number: number;
  target_number: number;
  e2e_number: number;
  user_app_props: ActorProperty[] = [];
  user_app_args: string[] = [];
  rank_argument: string = "";

  constructor(
    main_host: string,
    use_VNs: number | number[],
    process_per_node: number,
    initiator_number: number,
    target_number: number,
    e2e_number: number,
    user_app_props: ActorProperty[],
    args: string | string[] = [],
    rank_argument: string = "",
  ) {
    this.main_host = main_host;
    this.use_VNs = Array.isArray(use_VNs) ? use_VNs : [use_VNs];
    this.process_per_node = process_per_node;
    this.initiator_number = initiator_number;
    this.target_number = target_number;
    this.e2e_number = e2e_number;
    this.user_app_props = user_app_props;
    this.user_app_args = Array.isArray(args) ? args : [args];
    this.rank_argument = rank_argument;
  }

  get actors(): Actor[] {
    const actors = [];
    for (let i = 0; i < this.process_per_node; ++i) {
      const actorArguments = [...this.user_app_args];

      if (this.rank_argument?.length) {
        actorArguments.push(this.rank_argument);
        actorArguments.push("" + (rankCount++));
      }

      actors.push(
        new Actor(
          `${this.main_host}_CPU${i}`,
          ActorFunction.UserApp,
          this.user_app_props,
          actorArguments,
        ),
      );
    }

    this.use_VNs.map((vn) => {
      for (let i = 0; i < this.initiator_number; ++i) {
        actors.push(
          new Actor(
            this.main_host + "_NIC",
            ActorFunction.NicInitiator,
            [{ id: "VN", value: vn }],
          ),
        );
      }
      for (let i = 0; i < this.target_number; ++i) {
        actors.push(
          new Actor(
            this.main_host + "_NIC",
            ActorFunction.NicTarget,
            [{ id: "VN", value: vn }],
          ),
        );
      }
    });

    for (let i = 0; i < this.e2e_number; ++i) {
      actors.push(new Actor(this.main_host + "_NIC", ActorFunction.NicE2E, []));
    }

    return actors;
  }

  toString(): string {
    let nodeStr = `<!-- Host ${this.main_host} -->\n`;
    this.actors.map((a) => (nodeStr += a.toString() + "\n"));

    return nodeStr;
  }
}

export class S4BXIDeploy {
  nodes: DeployNode[];

  constructor(nodes: DeployNode[] = []) {
    this.nodes = nodes;
  }

  toString(): string {
    let nodesString = "";
    this.nodes.map((node) => (nodesString += node.toString() + "\n"));

    return `<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
${nodesString}</platform>`;
  }
}

export default { ActorFunction, Actor, DeployNode, S4BXIDeploy };
