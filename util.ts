/**
 * Range parser stolen from https://github.com/euank/node-parse-numeric-range/blob/master/index.js
 */
const parseNumberRange = (str: string): number[] => {
  const res = [];
  let m: any;

  // just a number
  if (/^-?\d+$/.test(str)) {
    res.push(parseInt(str, 10));
  } else if (
    (m = str.match(/^(-?\d+)(-|\.\.\.?|\u2025|\u2026|\u22EF)(-?\d+)$/))
  ) {
    // 1-5 or 1..5 (equivalent) or 1...5 (doesn't include 5)
    let [_, lhs, sep, rhs]: any = m;

    if (lhs && rhs) {
      lhs = parseInt(lhs);
      rhs = parseInt(rhs);
      const incr = lhs < rhs ? 1 : -1;

      // Make it inclusive by moving the right 'stop-point' away by one.
      if (sep === "-" || sep === ".." || sep === "\u2025") rhs += incr;

      for (let i = lhs; i !== rhs; i += incr) res.push(i);
    }
  }

  return res;
};

/**
 * Parser for hostname ranges
 *
 * Supported hostnames are <string> and <string><number>,
 * so for example "host" or "host42". Hence complete supported
 * format looks like "host1,host4..10,host42-69,host999"
 * (separator for)
 *
 * @param str Input string
 */
export const parseHostnameRange = (str: string): string[] => {
  const hostnames = [];

  for (const hostname of str.split(",").map((str: string) => str.trim())) {
    const splitHostname = hostname.split(/([0-9-\.]+)/).filter(s => s.length);

    if (splitHostname.length <= 1) {
      // Case "0" does not make sense but whatever
      hostnames.push(hostname);
      continue;
    }

    const nums = parseNumberRange(splitHostname[splitHostname.length - 1]);

    if (!nums.length) throw "Unsupported hostname format";

    nums.map((num: number) => hostnames.push(splitHostname.slice(0, splitHostname.length - 1).join('') + num));
  }

  return hostnames;
};

export const indent = (str = "", n = 4): string =>
  str.replace(/^(?!\s*$)/gm, " ".repeat(n));

export const numberMagnitudeFormat = (n: number): string => {
  const unitExponents: Array<any[]> = [
    [1e21, "Y"],
    [1e18, "E"],
    [1e15, "P"],
    [1e12, "T"],
    [1e9, "G"],
    [1e6, "M"],
    [1e3, "k"],
    [1, ""],
    [1e-3, "m"],
    [1e-6, "u"],
    [1e-9, "n"],
    [1e-12, "p"],
  ];

  for (const unit of unitExponents) {
    if (n >= unit[0]) {
      return `${n / unit[0]}${unit[1]}`;
    }
  }

  return `${n}`;
};

export default { parseHostnameRange, indent, numberMagnitudeFormat };
