import { Args, parse } from "https://deno.land/std@0.79.0/flags/mod.ts";
import { parseHostnameRange } from "./util.ts";
import { ActorProperty, DeployNode, S4BXIDeploy } from "./s4bxiDeploy.ts";

const args: Args = parse(Deno.args, {
  alias: {
    node: "n",
    "use-real-memory": "u",
    "model-pci": "m",
    "process-per-node": "p",
    "initiator-number": "i",
    "target-number": "t",
    "e2e-number": "e",
    argument: "a",
    "rank-argument": "ra",
  },
  boolean: ["use-real-memory", "model-pci"],
  string: ["node", "argument", "rank-argument"],
  default: {
    "use-real-memory": true,
    "model-pci": true,
    node: [],
    VN: [1, 3],
    "process-per-node": 1,
    "initiator-number": 2,
    "target-number": 2,
    "e2e-number": 1,
    argument: [],
    "rank-argument": ""
  },
});

const user_app_props: ActorProperty[] = [
  { id: "use_real_memory", value: args["use-real-memory"] },
  { id: "model_pci", value: args["model-pci"] },
];

const nodes: DeployNode[] = [];

const makeNode = (node: string): DeployNode => {
  return new DeployNode(
    node,
    args.VN,
    args["process-per-node"],
    args["initiator-number"],
    args["target-number"],
    args["e2e-number"],
    user_app_props,
    args.a,
    args["rank-argument"]
  );
}

const addNodes = (nodes: DeployNode[], str: string) =>
  parseHostnameRange(str).map((s: string) => nodes.push(makeNode(s)));

if (Array.isArray(args.node)) {
  args.node.map((n: string) => addNodes(nodes, n));
} else {
  addNodes(nodes, args.node);
}

console.log(new S4BXIDeploy(nodes).toString());
